"""
Providing datastructures for torrent
"""
from __future__ import annotations

from dataclasses import dataclass
from pathlib import Path


@dataclass(slots=True)
class Torrent:
    """
    Stores relevant info of a torrent
    """
    hash: str
    """The hash is intended as a unique identifier"""
    magnet_uri: str
    """Can be used to add the Torrent to a client"""
    save_path: Path
    """Path of the Torrents save directory"""
    content_path: Path
    """Path that points directly to the torrent itself"""
    name: str
    """Display name of the Torrent"""
    category: str
    """Category of Torrent"""
    tags: list[str]
    """list of tags"""

    @classmethod
    def from_json(cls, json_obj) -> Torrent:
        """
        Load from
        :param json_obj: object that has been parsed with ``json.loads()``
        :return: Torrent
        """
        tags = list(filter(None, json_obj.get('tags').split(',')))
        return Torrent(
            hash=json_obj.get('hash'),
            magnet_uri=json_obj.get('magnet_uri'),
            save_path=Path(json_obj.get('save_path')),
            content_path=Path(json_obj.get('content_path')),
            name=json_obj.get('name'),
            category=json_obj.get('category'),
            tags=[t.strip() for t in tags],
        )

    @property
    def dumps_friendly(self) -> dict:
        d_obj = dict()
        for attr in self.__slots__:
            if attr == 'tags':
                d_obj[attr] = getattr(self, attr)
            else:
                d_obj[attr] = str(getattr(self, attr))
        return d_obj
