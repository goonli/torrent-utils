class FailedAuthorizeError(Exception):
    pass


class NoCredentialsError(Exception):
    pass


class NoCredentialsFileError(Exception):
    pass
